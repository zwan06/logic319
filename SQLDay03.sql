
--	SQL DAY 03


-- CREATE STORED PROCEDURE
CREATE PROCEDURE sp_RetrieveMahasiswa
	-- PARAMETER

AS
BEGIN
	SELECT name,address,email,nilai FROM mahasiswa
END

-- CARA MANAMPILKAN / EXEC / RUN STORED PROCEDURE
exec sp_RetrieveMahasiswa

-- CARA UBAH PROCEDURE
-- ALTER STORED PROCEDURE
ALTER PROCEDURE sp_RetrieveMahasiswa
	-- PARAMETER
	@id int,
	@name varchar(50)
AS
BEGIN
	SELECT name,address,email,nilai FROM mahasiswa
	where id = @id and name = @name
END

exec sp_RetrieveMahasiswa 2,'Abiyu'

-- FUNCTION
CREATE FUNCTION fn_total_mahasiswa
(
	@id int
)
RETURNS INT
AS
BEGIN
	DECLARE @hasil int
	SELECT @hasil = count(id) FROM mahasiswa
	RETURN @hasil
END



ALTER FUNCTION fn_total_mahasiswa
(
	@id int,
	@nama varchar(50)
)
RETURNS INT
AS
BEGIN
	DECLARE @hasil int
	SELECT @hasil = count(id) FROM mahasiswa WHERE id = @id and name = @nama
	RETURN @hasil
END

SELECT dbo.fn_total_mahasiswa(2,'Abiyu')

