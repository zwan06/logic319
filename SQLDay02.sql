

-- SQL DAY 02

-- ADD Kolom Nilai
ALTER TABLE mahasiswa ADD nilai INT
-- UPDATE KOlom Nilai
UPDATE mahasiswa SET nilai = 80 WHERE id=1
UPDATE mahasiswa SET nilai = 90 WHERE id=2
UPDATE mahasiswa SET nilai = 70 WHERE id=3
UPDATE mahasiswa SET nilai = 80 WHERE id=4
UPDATE mahasiswa SET nilai = 60 WHERE id=5
UPDATE mahasiswa SET nilai = 70 WHERE id=6
UPDATE mahasiswa SET nilai = 75 WHERE id=7

-- MIN MAX COUNT
SELECT * FROM mahasiswa
-- MIN
SELECT MIN(id) FROM mahasiswa
-- MAX
SELECT MAX(id) FROM mahasiswa
-- COUNT
SELECT COUNT(id) FROM mahasiswa -- WHERE name = 'banu'
-- TOP
SELECT TOP 1 id FROM mahasiswa ORDER BY id ASC -- kalo mencari nilai terkecil pake ASC // DESC nilai terbesar

-- AVG SUM
-- AVG
SELECT AVG(nilai) FROM mahasiswa
-- SUM
SELECT SUM(nilai) FROM mahasiswa

-- SQL JOIN !!!

-- INNER JOIN
SELECT * FROM mahasiswa mhs INNER JOIN biodata bio ON mhs.id = bio.mahasiswa_id
-- RIGHT JOIN
SELECT * FROM mahasiswa mhs RIGHT JOIN biodata bio on mhs.id = bio.mahasiswa_id WHERE mhs.id IS NULL
-- Kondisi WHERE
SELECT * FROM mahasiswa mhs RIGHT JOIN biodata bio on mhs.id = bio.mahasiswa_id WHERE mhs.id IS NULL
-- LEFT JOIN
SELECT * FROM mahasiswa mhs LEFT JOIN biodata bio on mhs.id = bio.mahasiswa_id
-- Kondisi WHERE
SELECT * FROM mahasiswa mhs LEFT JOIN biodata bio on mhs.id = bio.mahasiswa_id WHERE bio.id IS NULL
-- FULL OUTER JOIN
SELECT * FROM mahasiswa mhs FULL OUTER JOIN biodata bio on mhs.id = bio.mahasiswa_id

-- DISTINCT 
SELECT DISTINCT name, email FROM mahasiswa

-- GROUP BY
SELECT name FROM mahasiswa GROUP BY name

-- SUBSTRING
SELECT SUBSTRING('SQL Tutorial',1,3) AS Hasil
SELECT SUBSTRING(name,1,3) AS Hasil FROM mahasiswa WHERE name = 'Abiyu'

-- CHARINDEX
SELECT CHARINDEX('t','Customer') AS Hasil

-- DATA LENGTH
SELECT DATALENGTH('W3Schools.Com') AS Hasil

-- CASE
SELECT name, address, 
CASE WHEN nilai >= 80 THEN 'A'
WHEN nilai >= 60 THEN 'B'
ELSE 'C'
END
AS IndexNilai FROM mahasiswa ORDER BY IndexNilai

-- CONCAT
SELECT CONCAT('SQL', 'IS','FUN')
SELECT 'HAA' + 'HIHI'

-- ARITMATIKA
SELECT 1 + 2

CREATE TABLE penjualan(
id INT PRIMARY KEY IDENTITY,
nama VARCHAR(50) NOT NULL,
harga INT NOT NULL
)

INSERT INTO penjualan (nama,harga) VALUES 
('Indomie', 1500),
('Close-Up', 3500),
('Pepsoden', 3000),
('Brush Formula', 2500),
('Roti Manis', 1000),
('Gula', 3500),
('Sarden', 4500),
('Rokok Sampurna', 11000),
('Rokok 234', 11000)

SELECT nama,harga,harga * 100 AS Total_Harga FROM penjualan  -- bisa pakai ini [Total Harga] Penganti AS

-- DATETIME 
-- GET DATE
SELECT GETDATE() [SEKARANG TGL]
-- DAY MONTH YEAR
SELECT DAY('2023/06/14') [TANGGAL]
SELECT MONTH('2023/06/14') [BULAN]
SELECT YEAR('2023/06/14') [TAHUN]
SELECT YEAR(GETDATE()) [TAHUN]

-- DATE ADD
SELECT DATEADD(YEAR,1,'2023/06/14') [TAHUN]
SELECT DATEADD(MONTH,1,'2023/06/14') [BULAN]
SELECT DATEADD(DAY,1,'2023/06/14') [TANGGAL]
SELECT DATEADD(DAY,1,GETDATE()) [TANGGAL]

-- DATEDIIF
SELECT DATEDIFF(YEAR,'2022/06/14','2023/01/30') [TAHUN]
SELECT DATEDIFF(MONTH,'2022/06/14','2023/01/30') [BULAN]
SELECT DATEDIFF(DAY,'2022/06/14','2023/01/30') [TANGGAL]
SELECT DATEDIFF(HOUR,'2022/06/14',GETDATE()) [JAM]
SELECT DATEDIFF(YEAR,GETDATE(),'2024/01/30') [TAHUN]

-- SUB QUERY
SELECT * FROM mahasiswa mhs
LEFT JOIN biodata bio ON mhs.id =  bio.mahasiswa_id 
WHERE mhs.nilai = (SELECT MAX(nilai) FROM mahasiswa) 

-- 
ALTER TABLE mahasiswa_new add nilai int
--
INSERT INTO mahasiswa_new(name,address,email,nilai)
SELECT name,address,email,nilai FROM mahasiswa
--
SELECT * FROM mahasiswa_new

-- CREATE VIEW
DROP VIEW vw_mahasiswa
CREATE VIEW vw_mahasiswa AS 
SELECT name FROM mahasiswa WHERE name = 'banu'

-- ALTER VIEW
ALTER VIEW vw_mahasiswa AS 
SELECT name,address,nilai FROM mahasiswa WHERE name != 'banu'

SELECT * FROM vw_mahasiswa

-- DATABASE INDEX
-- CREATE INDEX
CREATE INDEX index_nameemail ON mahasiswa(name,email)

-- CREATE UNIQUE INDEX
CREATE UNIQUE INDEX index_id ON mahasiswa(id)

-- PRIMARY KEY & UNIQUE KEY
CREATE TABLE coba (id int not null, nama varchar(50) not null)

ALTER TABLE coba ADD CONSTRAINT pk_idnama PRIMARY KEY(id,nama)

alter table coba alter column nama varchar(50) not null

-- UNIQUE KEY
ALTER TABLE biodata ADD CONSTRAINT unique_tgllahir UNIQUE(tgl_lahir)

-- DROP PRIMARY KEY & UNIQUE KEY
ALTER TABLE coba DROP CONSTRAINT pk_idnama --(Coba itu adalah nama tablenya pk_idnama itu adalah nama primary atau uniqe nya)

-- FOREIGN KEY
ALTER TABLE biodata ADD CONSTRAINT fk_mahasiswa_id FOREIGN KEY(mahasiswa_id) REFERENCES mahasiswa(id)



-- MISSION SUKSES