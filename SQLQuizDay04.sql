
-- SQL QUIZ DAY 04

CREATE DATABASE DB_HR

CREATE TABLE tb_karyawan(
id BIGINT PRIMARY KEY IDENTITY,
nip VARCHAR(50) NOT NULL,
nama_depan VARCHAR(50) NOT NULL,
nama_belajang VARCHAR(50) NOT NULL,
jenis_kelamin VARCHAR(50) NOT NULL,
agama VARCHAR(50) NOT NULL,
tempat_lahir VARCHAR(50) NOT NULL,
tgl_lahir DATE,
alamat VARCHAR(100) NOT NULL,
pendidikan_terakhir VARCHAR(50) NOT NULL,
tgl_masuk DATE
)

CREATE TABLE tb_divisi(
id BIGINT PRIMARY KEY IDENTITY,
kd_divisi VARCHAR(50) NOT NULL,
nama_divisi VARCHAR(50) NOT NULL
)

CREATE TABLE tb_jabatan(
id BIGINT PRIMARY KEY IDENTITY,
kd_jabatan VARCHAR(50) NOT NULL,
nama_jabatan VARCHAR(50) NOT NULL,
gaji_pokok INT,
tunjangan_jabatan INT
)

CREATE TABLE tb_pekerjaan(
id BIGINT PRIMARY KEY IDENTITY,
nip VARCHAR(50) NOT NULL,
kode_jabatan VARCHAR(50) NOT NULL,
kode_divisi VARCHAR(50) NOT NULL,
tunjangan_kinerja INT,
kota_penempatan VARCHAR(50)
)

INSERT INTO tb_karyawan
(nip,nama_depan,nama_belajang,jenis_kelamin,agama,tempat_lahir,tgl_lahir,alamat,pendidikan_terakhir,tgl_masuk)
VALUES
('001','Hamidi','Samsudin','Pria','Islam','Sukabumi','1977-04-21','Jl. Sudirman No. 12','S1 Teknik Mesin','2015-12-07'),
('002','Paul','Christian','Pria','Kristen','Ambon','1980-05-27','Jl. Veteran No. 4','S1 Pendidikan Geografi','2014-01-12'),
('003','Ghandi','Wamida','Wanita','Islam','Palu','1992-01-12','Jl. Rambutan No. 22','SMA Negeri 02 Palu','2014-12-01')

INSERT INTO tb_divisi
(kd_divisi,nama_divisi) VALUES
('GD','Gudang'),
('HRD','HRD'),
('KU','Keuangan'),
('UM','Umum')

INSERT INTO tb_jabatan
(kd_jabatan,nama_jabatan,gaji_pokok,tunjangan_jabatan) VALUES
('MGR','Manager',5500000,1500000),
('OB','Office Boy',1900000,200000),
('ST','Staff',3000000,750000),
('WMGR','Wakil Manager',4000000,1200000)

INSERT INTO tb_pekerjaan (nip,kode_jabatan,kode_divisi,tunjangan_kinerja,kota_penempatan) VALUES
('001','ST','KU',750000,'Cianjur'),
('002','OB','UM',350000,'Sukabumi'),
('003','MGR','HRD',1500000,'Sukabumi')

-- 1. Menampilkan yang gaji + tunjangan kinerja dibawah 5juta
SELECT nama_depan + nama_belajang AS nama_lengkap,nama_jabatan,gaji_pokok + tunjangan_jabatan AS gaji_tunjangan FROM tb_karyawan AS k 
JOIN tb_pekerjaan AS p ON k.nip = p.nip 
JOIN tb_jabatan AS j ON p.kode_jabatan = j.kd_jabatan
WHERE gaji_pokok + tunjangan_kinerja < 5000000

-- 2. 
SELECT nama_depan + nama_belajang AS nama_lengkap, nama_jabatan, 
gaji_pokok + tunjangan_jabatan + tunjangan_kinerja AS total_gaji,
0.5 * (gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) AS pajak,
gaji_pokok + tunjangan_jabatan + tunjangan_kinerja - 0.5 * (gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) AS gaji_bersih
FROM tb_karyawan AS k
JOIN tb_pekerjaan AS p ON k.nip = p.nip 
JOIN tb_jabatan AS j ON p.kode_jabatan = j.kd_jabatan
WHERE gaji_pokok + tunjangan_kinerja < 5000000