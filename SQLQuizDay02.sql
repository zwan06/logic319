

-- SQL Quiz Day 02

CREATE DATABASE DB_Entertainer

CREATE TABLE artis(
kd_artis VARCHAR(10) PRIMARY KEY NOT NULL,
nm_artis VARCHAR(100) NOT NULL,
jk VARCHAR(100) NOT NULL,
bayaran BIGINT NOT NULL,
award INT NOT NULL,
negara VARCHAR(100) NOT NULL
)

CREATE TABLE film(
kd_film VARCHAR(10) PRIMARY KEY NOT NULL,
nm_film VARCHAR(55) NOT NULL,
genre VARCHAR(55) NOT NULL,
artis VARCHAR(55) NOT NULL,
produser VARCHAR(55) NOT NULL,
pendapatan BIGINT NOT NULL,
nominasi INT NOT NULL
)

CREATE TABLE produser (
kd_produser VARCHAR(50) PRIMARY KEY NOT NULL,
nm_produser VARCHAR(50) NOT NULL,
international VARCHAR(50) NOT NULL
)

CREATE TABLE negara(
kd_negara VARCHAR(100) PRIMARY KEY NOT NULL,
nm_negara VARCHAR(100) NOT NULL
)

CREATE TABLE genre(
kd_genre VARCHAR(50) PRIMARY KEY NOT NULL,
nm_genre VARCHAR(50) NOT NULL
)

INSERT INTO artis (kd_artis,nm_artis,jk,bayaran,award,negara) VALUES
('A001','ROBERT DOWNEY JR','PRIA',3000000000,2,'AS'),
('A002','ANGGELINA JOLIE','WANITA',700000000,2,'AS'),
('A003','JACKIE CHAN','PRIA',200000000,2,'HK'),
('A004','JOE TASLIM','PRIA',350000000,2,'ID'),
('A005','CHELSEA ISLAN','WANITA',300000000,2,'ID')

INSERT INTO film(kd_film,nm_film,genre,artis,produser,pendapatan,nominasi) VALUES
('F001','IRON MAN','G001','A001','PD01',2000000000,3),
('F002','IRON MAN 2','G001','A001','PD01',1800000000,2),
('F003','IRON MAN 3','G001','A001','PD01',1200000000,0),
('F004','AVENGER CIVIL WAR','G001','A001','PD01',2000000000,1),
('F005','SPIDERMAN HOME COMING','G001','A001','PD01',1300000000,0),
('F006','THE RAID','G001','A004','PD03',800000000,5),
('F007','FAST & FURIOUS','G001','A001','PD05',830000000,2),
('F008','HABIBIE DAN AINUN','G004','A005','PD03',670000000,4),
('F009','POLICE STORY','G001','A003','PD02',700000000,3),
('F010','POLICE STORY 2','G001','A003','PD02',710000000,1),
('F011','POLICE STORY 3','G001','A003','PD02',615000000,0),
('F012','RUSH HOUR','G003','A003','PD05',695000000,2),
('F013','KUNGFU PANDA','G003','A003','PD05',2000000000,5)

INSERT INTO produser(kd_produser,nm_produser,international) VALUES
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAPI FILM','TIDAK'),
('PD04','PARKIT','TIDAK'),
('PD05','PARAMOUNT PICTURE','YA')

INSERT INTO negara (kd_negara,nm_negara) VALUES
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')

INSERT INTO genre (kd_genre,nm_genre) VALUES
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILLER'),
('G006','FICTION')

-- 1. Menampilkan jumlah pendapatan produser marven secara keseluruan
SELECT p.nm_produser, SUM(f.pendapatan) FROM produser AS p JOIN film AS f ON p.kd_produser = f.produser
WHERE p.nm_produser = 'MARVEL' GROUP BY p.nm_produser

-- 2. Menampilkan Film dan Nominasi
SELECT nm_film, nominasi FROM film WHERE nominasi < 1

-- 3. Menampilkan Nama film yang depan huruf depannya p
SELECT nm_film FROM film WHERE nm_film LIKE 'p%'

SELECT nm_film FROM film WHERE SUBSTRING(nm_film,1,1) = 'p'

-- 4. Menampilkan Nama Film yang huruf terakhirnya y
SELECT nm_film FROM film WHERE nm_film LIKE '%y'

-- 5. Menampilkan Nama Film Yang Mengandung huruf d
SELECT nm_film FROM film WHERE nm_film LIKE '%d%'

-- 6. Menampilkan Nama Film Dan Artis
SELECT f.nm_film, a.nm_artis FROM film AS f 
JOIN artis AS a ON f.artis = a.kd_artis 

-- 7. Menampilkan nama film yang artisnya berasal dari negara hongkong
SELECT f.nm_film, n.kd_negara,nm_negara FROM film AS f 
JOIN artis AS a ON f.artis = a.kd_artis
JOIN negara AS n ON n.kd_negara = a.negara 
WHERE n.kd_negara = 'HK'

-- 8. Menampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf o
SELECT f.nm_film, n.nm_negara FROM film AS f 
JOIN artis AS a ON f.artis = a.kd_artis
JOIN negara AS n ON n.kd_negara = a.negara 
WHERE n.nm_negara NOT LIKE '%o%' 

-- 9. Menampilkan nama artis yang tidak pernah bermain film
SELECT a.nm_artis FROM artis AS a LEFT JOIN film AS f ON a.kd_artis = f.artis
WHERE f.artis IS NULL

-- 10. Menampilkan nama artis yang bermain film dengan genre Drama
SELECT a.nm_artis, g.nm_genre FROM artis AS a 
JOIN film AS f ON a.kd_artis = f.artis
JOIN genre AS g ON f.genre = g.kd_genre 
WHERE g.nm_genre = 'DRAMA'

-- 11. Menampilkan nama artis yang bermain film dengan genre Action
SELECT DISTINCT a.nm_artis, g.nm_genre FROM artis AS a 
JOIN film AS f ON a.kd_artis = f.artis
JOIN genre AS g ON f.genre = g.kd_genre 
WHERE g.nm_genre = 'ACTION' 
-- GROUP BY a.nm_artis, g.nm_genre

-- 12. Menampilkan data negara dengan jumlah Filemnya
SELECT n.kd_negara, n.nm_negara, COUNT(f.nm_film) AS jumlah_film FROM negara AS n 
LEFT JOIN artis AS a ON n.kd_negara = a.negara 
LEFT JOIN film AS f ON a.kd_artis = f.artis
GROUP BY n.nm_negara, n.kd_negara
ORDER BY n.nm_negara

SELECT n.kd_negara, n.nm_negara, COUNT(f.nm_film) AS jumlah_film FROM artis AS a 
JOIN film AS f ON a.kd_artis = f.artis 
RIGHT JOIN negara AS n ON a.negara = f.kd_film
GROUP BY n.nm_negara, n.kd_negara
ORDER BY n.nm_negara

-- 13. Menampilkan nama film yang skala internasional 
SELECT f.nm_film FROM film AS f 
JOIN produser as p ON f.produser = p.kd_produser
WHERE p.international = 'YA'

-- 14. Menampilkan Jumlah Film dari masing masing produser
SELECT p.nm_produser, COUNT(f.nm_film) AS jumlah_film FROM produser AS p
LEFT JOIN  film AS f ON p.kd_produser = f.produser 
GROUP BY p.nm_produser HAVING p.nm_produser LIKE '____e%'

